﻿using System.ComponentModel.DataAnnotations;
namespace Seldat {
    public class AdminModel {

        public int id { get; set; }

        [RegularExpression("^[a-zA-Z0-9]{4,8}$", ErrorMessage = "password must be 4-8 chaters")]
        [Required(ErrorMessage = "password is missing")]
        public string password { get; set; }

        [Required(ErrorMessage = "name is missing")]
        public string name { get; set; }
    }
}
