﻿using System;

namespace Seldat {
    public class BaseLogic : IDisposable {

        protected ClothingStoreContext db = new ClothingStoreContext();
        public void Dispose() {
            db.Dispose();
        }
    }
}
