﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class ItemLogic : BaseLogic {

        public List<ItemModel> GetAllItems() {
            return db.Items.Select(i => new ItemModel { id = i.ItemId, name = i.ItemName }).ToList();
        }

        public List<ItemModel> GetTypeByCategory(int categoryId) {
            return db.Categories.Where(c => c.CategoryId == categoryId).FirstOrDefault().Items.Select(i => new ItemModel { id = i.ItemId, name = i.ItemName }).ToList();
        }

    }
}
