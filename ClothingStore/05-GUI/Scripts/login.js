﻿function adminLogin() {
    var password = $("#pass").val();
    var name = $("#name").val();
    $.ajax({
        method: "POST",
        url: "http://localhost:58792/api/admin",
        data: { name: name, password: password },
        cache: false,
        error: function (err) {
            var errors = "";
            errors += err.responseJSON.password;
            alert(errors);
        },
        success: function (response) {
            if (response == true) {
                sessionStorage.setItem("user", name);
                window.location.href = "admin.html";
            }
            else
                window.location.href = "index.html";
        }
    });
}
