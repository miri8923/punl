﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class ItemApiController : ApiController {

        ItemLogic logic = new ItemLogic();

        [HttpGet]
        [Route("api/item")]
        public HttpResponseMessage GetAllItems() {
            try {
                List<ItemModel> items = logic.GetAllItems();
                return Request.CreateResponse(HttpStatusCode.OK, items);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }


        [HttpGet]
        [Route("api/items/category/{categoryId}")]
        public HttpResponseMessage GetItemsByCategory([FromUri]int categoryId) {
            try {
                List<ItemModel> items = logic.GetTypeByCategory(categoryId);
                return Request.CreateResponse(HttpStatusCode.OK, items);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFreindlyMessage());
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}
