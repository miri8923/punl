﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seldat {
  public class ClothingStoreContext:DbContext {
        public ClothingStoreContext() : base("Clothing")
        { }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Cloth> Clothes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            Database.SetInitializer<ClothingStoreContext>(new DropCreateDatabaseIfModelChanges<ClothingStoreContext>());
            base.OnModelCreating(modelBuilder);
        }
    }
}
